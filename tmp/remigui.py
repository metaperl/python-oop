

import remi.gui as gui
from remi import start, App
from threading import Timer
import pony.orm as pny
import models


class MyApp(App):
    def __init__(self, *args):
        super(MyApp, self).__init__(*args)

    def idle(self):
        self.render_table()


    def render_table(self):
        table_data = models.table_of_artists()
        self.table.empty()
        self.table.append_from_list(table_data)

        self.table.on_table_row_click.connect(self.on_table_row_click)

    def main(self):
        # the margin 0px auto centers the main container
        verticalContainer = gui.Widget(width=540, margin='0px auto', style={'display': 'block', 'overflow': 'hidden'})

        horizontalContainer = gui.Widget(width='100%', layout_orientation=gui.Widget.LAYOUT_HORIZONTAL, margin='0px',
                                         style={'display': 'block', 'overflow': 'auto'})

        subContainerLeft = gui.Widget(width=320, style={'display': 'block', 'overflow': 'auto', 'text-align': 'center'})
        self.img = gui.Image('/res/logo.png', height=100, margin='10px')
        self.img.onclick.connect(self.on_img_clicked)

        self.table = gui.Table(width=300, height=200, margin='10px')

        self.render_table()

        subContainerLeft.append([self.img, self.table])

        #horizontalContainer.append([subContainerLeft, subContainerRight])
        horizontalContainer.append([subContainerLeft])

        menu = gui.Menu(width='100%', height='30px')
        m1 = gui.MenuItem('File', width=100, height=30)
        m2 = gui.MenuItem('View', width=100, height=30)
        m11 = gui.MenuItem('Save', width=100, height=30)
        m12 = gui.MenuItem('Open', width=100, height=30)
        m111 = gui.MenuItem('Save', width=100, height=30)
        m112 = gui.MenuItem('Save as', width=100, height=30)
        m3 = gui.MenuItem('Add Artist', width=100, height=30)
        m3.onclick.connect(self.menu_dialog_clicked)

        menu.append([m1, m2, m3])
        m1.append([m11, m12])
        m11.append([m111, m112])

        menubar = gui.MenuBar(width='100%', height='30px')
        menubar.append(menu)

        verticalContainer.append([menubar, horizontalContainer])

        # returning the root widget
        return verticalContainer

    def menu_dialog_clicked(self, widget):
        self.dialog = gui.GenericDialog(title='Dialog Box', message='Click Ok to transfer content to main page',
                                        width='500px')
        self.dtextinput = gui.TextInput(width=200, height=30)
        self.dtextinput.set_value('(artist name)')
        self.dialog.add_field_with_label('dtextinput', 'Musical Artist Name', self.dtextinput)

        self.dialog.confirm_dialog.connect(self.dialog_confirm)
        self.dialog.show(self)

    def dialog_confirm(self, widget):
        result = self.dialog.get_field('dtextinput').get_value()
        self.model_artist_save(result)
        self.render_table()

    # listener function
    def on_img_clicked(self, widget):
        self.lbl.set_text('Image clicked!')

    def on_table_row_click(self, table, row, item):
        self.lbl.set_text('Table Item clicked: ' + item.get_text())

    def model_artist_save(self, artist_name):
        with pny.db_session:
            _ = models.Artist(name=artist_name)

    def on_close(self):
        """ Overloading App.on_close event to stop the Timer.
        """
        self.stop_flag = True
        super(MyApp, self).on_close()


if __name__ == "__main__":
    # starts the webserver
    # optional parameters
    # start(MyApp,address='127.0.0.1', port=8081, multiple_instance=False,enable_file_cache=True, update_interval=0.1, start_browser=True)
    import ssl

    start(MyApp, debug=True, address='0.0.0.0', port=61000, start_browser=True, multiple_instance=False)
