# python-oop

Python Object-Oriented Programming resources.

All information is in
[the wiki](https://gitlab.com/metaperl/python-oop/wikis/home).
